function getInputElement(acronym, side) {
  var elements = document.getElementsByClassName('label-diameter-input')
  for (var i = 0; i < elements.length; i++) {
    if (elements[i].dataset.acronym === acronym && elements[i].dataset.side === side) {
      return elements[i]
    }
  }
}

function computeDiamterString(element) {
  // if the field is zero that means that nothing has been input, or is invalid (characters instead of numbers)
  if (parseInt(element.dataset.micrometers, 10) !== 0) {
    if (element.dataset.unitsType === 'cm' && element.dataset.centimeters) {
      return element.dataset.centimeters + 'cm'
    } else if (element.dataset.unitsType === 'mm' && element.dataset.millimeters) {
      return element.dataset.millimeters + 'mm'
    }
  }

  return ''
}

function addDiameterEventListeners() {
  var elements = document.getElementsByClassName('label-diameter-input')
  for (var i = 0; i < elements.length; i++) {
    elements[i].addEventListener('input', diameterInputEvent)
  }

  var elements = document.getElementsByClassName('diameter-units-select')
  if (elements.length === 1) {
    elements[0].addEventListener('change', diameterUnitsChangeEvent)
  }
}

/*
  Get the form element measurement units (select dropdown or radio button)
  Default set to centimeters if no form element found.
*/
function getDiameterUnits() {
  var elements = document.getElementsByClassName('diameter-units-select');
  if (elements.length === 1) {
    return elements[0].value;
  }

  return "cm"
}

/*
  Add four data attributes to diameter inputs. 
  
  - One for each unit type:
    'data-micrometers', 'data-centimeters', 'data-millimeters'
  - and one for what the last set units was:
    'data-units-type' ('cm' || 'mm')

  Event for when the user types or changes the diameter input.
*/
function updateDiameterAttributes(input) {
  if (input.tagName.toLowerCase() !== 'input') {
    throw new Error(`Not an input element!`)
  }

  var units = getDiameterUnits();
  var value = validateDiameter(input.value);

  if (units === 'cm') {
    var micrometers = centimetersToMicrometers(value);
    input.setAttribute('data-centimeters', '' + value)
    input.setAttribute('data-micrometers', '' + micrometers)
    input.setAttribute('data-millimeters', '' + micrometersToMillimeters(micrometers))
  } else if (units === 'mm') {
    var micrometers = millimetersToMicrometers(value)
    input.setAttribute('data-centimeters', '' + micrometersToCentimeters(micrometers))
    input.setAttribute('data-micrometers', '' + micrometers)
    input.setAttribute('data-millimeters', '' + value)
  }

  input.setAttribute('data-units-type', units)
}


/*
  Force a diameter measurement to a float type.
  
  @parameter {Any}
  @return {Float}
*/
function validateDiameter(diameter) {
  var float = parseFloat(diameter, 10)
  if (Number.isNaN(float)) {
    return 0;
  }
  return float;
}

/*
  Unit conversion. μm to cm. 
  Micrometers (10 ^ -6) to centimeters (10 ^ -2)

  @param {Float} micrometers
  @return {Float}
*/
function micrometersToCentimeters(micrometers) {
  return (micrometers / 10000);
}

/*
  Unit conversion. μm to cm. 
  Micrometers (10 ^ -6) to millimeters (10 ^ -3)

  @param {Float} micrometers
  @return {Float}
*/
function micrometersToMillimeters(micrometers) {
  return (micrometers / 1000)
}

/*
  Unit conversion. cm to μm. 
  Centimeters (10 ^ -2) to micrometers (10 ^ -6)

  @param {Float} centimeters
  @return {Int}
*/
function centimetersToMicrometers(cm) {
  return parseInt(cm * 10000, 10);
}


/*
  Unit conversion. mm to μm. 
  Millimeters (10 ^ -3) to micrometers (10 ^ -6)

  @param {Float} millimeters
  @return {Int}
*/
function millimetersToMicrometers(mm) {
  return parseInt(mm * 1000, 10);
}

/* Event handlers */

/*
  Event fired on text input for vein diameters.
*/
function diameterInputEvent(e) {

  updateDiameterAttributes(e.srcElement);

  var event = new CustomEvent('label-diameter-update', {
    detail: {
      acronym: e.srcElement.dataset.acronym,
      side: e.srcElement.dataset.side,
      diameter: computeDiamterString(e.srcElement)
    }
  })

  e.srcElement.dispatchEvent(event);
}

/*
    Update all the diameter inputs value's to whatever the current unit selection is. 
    Fired on selection event.
*/
function diameterUnitsChangeEvent(e) {
  var inputs = document.getElementsByClassName('label-diameter-input');
  for (var i = 0; i < inputs.length; i++) {
    var units = getDiameterUnits();

    if (inputs[i].tagName.toLowerCase() !== 'input') { // verify that it is an input HTML tag
      throw new Error(`Not an input element!`)
    }

    var val = '';
    if (units === 'mm') {
      val = inputs[i].dataset.millimeters;
    } else if (units === 'cm') {
      val = inputs[i].dataset.centimeters;
    }

    inputs[i].setAttribute('data-units-type', units)
    inputs[i].value = val

    // needs to fire event to update the drawing label
    var event = new CustomEvent('label-diameter-update', {
      detail: {
        acronym: inputs[i].dataset.acronym,
        side: inputs[i].dataset.side,
        diameter: computeDiamterString(inputs[i])
      }
    })
  
    inputs[i].dispatchEvent(event);

  }
}